def combinatii(cifra):
    if cifra == "":
        return []
    string_maps = {
        "1": "abc",
        "2": "def",
        "3": "ghi",
        "4": "jkl",
        "5": "mno",
        "6": "pqrs",
        "7": "tuv",
        "8": "wxy",
        "9": "z"
    }
    rez = [""]
    for n in cifra:
        tmp = []
        for z in rez:
            for char in string_maps[n]:
                tmp.append(z + char)
        rez = tmp
    return rez

lista = "12"
print(combinatii(lista))
lista = "24"
print(combinatii(lista))
