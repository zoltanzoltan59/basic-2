def generate_stro(n):
    """
    :type n: int
    :rtype: List[str]
    """

    rez = helper(n, n)
    return rez

def helper(n, l):
    if n == 0:
        return [""]
    if n == 1:
        return ["1", "0", "8"]
    mij = helper(n-2, l)
    rez = []
    for m in mij:
        if n != l:
            rez.append("0" + m + "0")
        rez.append("8" + m + "8")
        rez.append("1" + m + "1")
        rez.append("9" + m + "9")
        rez.append("6" + m + "6")
    return rez

print("n = 2: \n",generate_stro(2))
print("n = 3: \n",generate_stro(3))
print("n = 4: \n",generate_stro(4))
