import collections
import pprint

fisier = input('Nume Fisier: ')
with open(fisier, 'r') as i:
    cnt = collections.Counter(i.read().upper())
    valoare = pprint.pformat(cnt)
print(valoare)
