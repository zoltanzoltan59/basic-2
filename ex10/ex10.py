import platform as pl

profil_platforma = ['architecture', 'machine', 'platform', 'process', 'python_build', 'python_compiler', 'python_version', 'uname', 'version']

for key in profil_platforma:
    if hasattr(pl, key):
        print(key + ": " + str(getattr(pl, key)()))
