Write a Python program to add two positive integers without using the '+' operator.

Note: Use bitwise operations to add two numbers.
